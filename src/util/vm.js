/**
 * @Date:   2017-08-27T12:13:39+08:00
 * @Last modified time: 2017-08-27T20:20:21+08:00
 */
'use strict';
var Hogan = require('hogan.js');
var conf = {
  serverHost:''
};
var _vm = {
  request:function(param){
    var _this = this;
    $.ajax({
      type:param.method ||'get',
      url:param.url||'',
      dataType:param.type||'json',
      data:param.data||'',
      success:function(res){
        // 请求成功
        if(0===res.status){
          typeof param.success ==='function' && param.success(res.data,res.msg);
        }
        // 没有登录状态需要强制登陆
        else if(10=== res.status){
          _this.doLogin();
        }
        // 参数错误
        else if(1===res.status){
          typeof param.error ==='function' && param.error(res.msg);
        }
      },
      error:function(err){
        typeof param.error ==='function' && param.error(err.statusText);
      }
    });
  },
  // 获取服务器地址
  getServerUrl:function(path){
    return conf.serverHost + path;
  },
  // 获取 url 参数
  getUrlParam:function(name){
    var reg = new RegExp('(^|&)'+name+'=([^&]*)(&|$)');
    var result = window.location.search.substr(1).match(reg);
    return result ? decodeURIComponent(result[2]) : null;
  },
  // 渲染html模板
  renderHtml:function(htmlTemplete,data){
    var templete = Hogan.compile(htmlTemplete),
    result  = templete.render(data);
    return result;
  },
  // 成功提示
  successTips:function(msg){
    alert(msg || '操作成功! ');
  },
  errorTips:function(msg){
    alert(msg || '哪里出错了! ');
  },
  // 字段的验证,支持非空判断,手机,邮箱
  validate : function(value,type){
    var value = $.trim(value);
    // 非空验证, value强转成布尔值,有值返回true,没有值返回false
    if('require' ===type){
      return !!value;
    }
    // 手机号验证
    if('phone' === type){
      return /^1\d{10}$/.test(value);
    }
    // 邮箱
    if('email' === type){
      return  /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/.test(value);
    }

  },
  // 统一登录处理
  doLogin:function(){
    window.location.href = './login.html?redirect=' + encodeURIComponent(window.location.href);
  },
  goHome:function(){
    window.location.href = './index.html';
  }
};

module.exports = _vm;
