/**
 * @Date:   2017-08-25T12:01:41+08:00
 * @Last modified time: 2017-09-01T17:59:40+08:00
 */
'use strict';
require('./index.css');
var _vm = require('util/vm.js');
var templateIndex = require('./index.string');
/* 侧边导航 */
var navSide = {
  option: {
    name: '',
    navList: [{
      name: 'user-center',
      desc: '个人中心',
      href: './user-center.html'
    }, {
      name: 'order-list',
      desc: '我的订单',
      href: './order-list.html'
    }, {
      name: 'pass-update',
      desc: '修改密码',
      href: './pass-update.html'
    }, {
      name: 'about',
      desc: '关于VMall',
      href: './about.html'
    }]
  },
  init: function(option) {
    // 合并选项
    $.extend(this.option,option);
    this.renderNav();
  },
  // 渲染导航数据
  renderNav: function() {
    // 计算 active 数据
    for(var i = 0,iLength = this.option.navList.length; i < iLength; i++){
      if(this.option.navList[i].name === this.option.name){
        this.option.navList[i].isActive = true;
      }
    };
    // 渲染 list 数据
    var navHtml = _vm.renderHtml(templateIndex,{
      navList : this.option.navList
    });
    // html 放入容器里
    $('.nav-side').html(navHtml);
  }
};
module.exports = navSide;
