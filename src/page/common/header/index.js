/**
 * @Date:   2017-09-01T11:51:08+08:00
 * @Last modified time: 2017-09-01T12:27:52+08:00
 */
'use strict';
require('./index.css');
var _vm = require('util/vm.js');
// 通用页面头部
var header = {
  init:function(){
     this.bindEvent();
  },
  onload:function(){
    var keyword = _vm.getUrlParam('keyword');
    if(keyword){
      $('#search-input').val(keyword);
    }
  },
  bindEvent:function(){
    var _this = this;
    // 点击搜索
    $('#search-btn').click(function(){
      _this.searchSubmit();
    });
    // 按回车
    $('#search-input').keyup(function(e){
      if(e.keyCode === 13){
        _this.searchSubmit();
      }
    });
  },
  // 搜索的提交
  searchSubmit: function(){
    var keyword = $.trim($('#search-input').val());
    if(keyword){
      window.location.href = './list.html?keyword='+keyword;
    }else {
      _vm.goHome();
    }
  }
};
// 不需要给外部使用 module.exports = header.init();
header.init();
