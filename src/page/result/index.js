/**
 * @Date:   2017-09-02T19:32:16+08:00
 * @Last modified time: 2017-09-02T19:58:43+08:00
 */
'use strict';
require('./index.css');
require('page/common/nav-simple/index.js');
var _vm = require('util/vm.js');

$(function(){
  var type = _vm.getUrlParam('type') || 'default',
    $element = $('.'+type+'-success');
  $element.show();
})
